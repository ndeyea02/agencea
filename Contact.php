<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>AGENCEA</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="shortcut icon" href="public\images\logoCom.png">
   
       <style>
     body{
        background-image: url("public/images/FOND5.png");
        padding: 0;
        position: fixed;
        background-color: white;
        font-family: "Times New Roman", Times, "Liberation Serif", FreeSerif, serif;
        font-size: 12px; 
        background-repeat:no-repeat;
      
        }
     html,body{
         height: auto;
         max-width:900px;
         margin:auto;

        
     }
     footer{
        padding-left: 1px;
        background-color: #FDC3AA;
        width: 100%;
        height: 50px;
        position: absolute;
        bottom: 0px;
        left: 0px;
        color: white;
     }
    .contenu{
            margin: 3px;
            padding-left: 40px;
            }
    .contenu li{
            list-style: none;
            margin: -19px;
            padding-left: 12px;
            }
    .droits{
                margin: -10px;
                padding-left: 10px;
            }
    .navbar{
                margin: 0;
                width: 100%;
                padding-left: 25px;
            }
    .contenu1{
            margin: 24px;
            padding-left: 100px;
            }

    .contenu1 li{
        list-style: none;
        margin: -18.7px;
        padding-left: 50px;
        font-size: 8px;
    }
    .contenu2{
        margin: 3px;
        padding-left: 190px;
    }
    .contenu2 li{
        list-style: none;
        margin: -20px;
        padding-left: 80px;
        font-size: 9px;
    }
    .contenu2 li a{
        list-style: none;
        margin: -18px;
        padding-left: 80px;
        font-size: 9px;
        text-decoration: none;
        color:white;
          }
          .contenu3{
        margin: -35px;
        padding-left: 650px;
        }
          .lien{
        margin: 25px;
        padding-left: 560px;
        font-size: 10px;
        color:white;
        }
    .li{
            color:white;
        }
    .remerciement{
            margin: -100px;
            padding-left: 810px;
            font-size: 13px;
            position:center;
        
        }
    .formulaire{
            max-height: 300px;
	        max-width: 400px;
            max-width: 370px;
            display:grid;
            justify-content: center;
            position: relative;
            background-size: cover; 
            margin:-45px;
            content:'';
            top:-21px;
            z-index: 6;
            color:#FDC3AA;
            padding-left: 60px;
          
        }
    .field{
            padding: 0.5rem 0.5rem;
            outline: none;
            border: 1.5px solid #FDC3AA;
            font-size: 0.9rem;
            margin-bottom: 8px;
            transition: 4s;
             color:#FDC3AA; 
            border-radius: 10px;
        }
        .field::placeholder{
            color:#FDC3AA;
        }
    .field1{
            outline: none;
            padding: 0.5rem 0.5rem;
            border: 1.5px solid #FDC3AA;
            font-size: 0.9rem;
            margin-bottom: 9px;
            transition: 4s;
            border-radius: 10px;
            width:260px;
            height:100px;
            color:#FDC3AA;
        }
        .field1::placeholder{
            color:#FDC3AA;
        }
    .illusta{
            margin:-90px;
            padding-left:10px;
            color:white;
            position:fixed;
        }
    .fond7{
            padding-left: 837.5px;
        }
    button {
            display: inline-block;
            background-color: #FDC3AA;
            border-radius: 10px;
            border: 2.5px solid white;
            font-size: 16px;
            width: 100px;
            cursor: pointer;
            margin: 0;
            color:white;
            }
    
    .listeLi_Footer{
                margin: 0;
            }
            #link { color:white ; }
            .nav-link{
        color:  #FDC3AA;}
       </style>
    </head>
<body>
<nav class="navbar navbar-expand-md" style="background-color:white" > 
    <a href="#"><img src="public\images\logoCom.png" alt="" width="100px" height="65px"></a>
    <button type="button" class="navbar-toggler"><span class="navbar-toggler-icon px-6" ></span></button>    
    <ul class="navbar-nav">
            <li class="nav-item"><a  class="nav-link font-weight-bold text-uppercase px-5" href="index.php">Accueil</a></li>
            <li class="nav-item"><a  class="nav-link font-weight-bold text-uppercase px-5" href="NosServices.php">Nos Services</a></li>
             <li class="nav-item"><a class="nav-link font-weight-bold text-uppercase px-5" href="#">Contact</a></li>
             <li class="nav-item"><a class="nav-link font-weight-bold text-uppercase px-5" href=""><img src="public\images\drapeau.jpg" alt="" width="14px" height="14px">FR</a></li>  
        </ul>
    </div>
</nav>
    <div class="bandeau">
    <img src="public\images\BANDEAU6.png" alt="" width="100%" height="87px">
    </div>
    <div class="fond7">
    <img src="public\images\FOND7.png" alt="" width="130px" height="80px">
    </div>
    <form action="commandeControllor.php" method="Post" class="formulaire">
       <h5><strong style="color: white;" >NOUS CONTACTER</strong></h5>
            <input type="text" name="nom" id="nom" class="field" required  placeholder="Nom D'entreprise">
            <span id='missNom'></span>
            <input type="text" class="field" name="mail" required  placeholder="Mail">
            <select type="text" class="field" name="domaine" required>
            <option value="" type="text" class="field" id=""  selected>Domaine d'expertise demandé</option>
            <option value="">Marketing et Communication </option>
            <option value="">Consulting</option>
            <option value="">Informatique</option>
            </select>
            <textarea class="field1" name="libelle" placeholder="Votre Demande"></textarea>
            <div class="button">
                <button type="submit" name ="enreg" id="enreg" >Envoyer</button>
             </div>
            
          
    </form> 
        <div class="illusta">
            <img src="public\images\PIED_PAGE_CONTACT2.png" alt="" width="960px" height="170px">
            <p class="remerciement">L'équipe Agencea vous remercie <br> de prendre contact avec nous. </p>
        </div>
        <footer>
            <div class="contenu">
                <img src="public\images\footer.png" alt="" width="35px" height="30px">
                <ul class="listeLi_Footer">
                        <li>Tous droits réserbés</li><br>
                        <li>© 2020 agencea</li>
                </ul>      
            </div>  
            <div class="contenu1">
                <ul>
                    <li>Mentions légale</li> <br>
                    <li>Politique de Confidentialité</li><br>
                    <li>Statistiques et Performances</li>  <br>
                    <li>Réclamation</li> 
                </ul>
            </div>
            <div class="contenu2">
                <ul>
                    <li><a href="index.php">Accueil</a></li><br>
                    <li><a href="NosServices.php">Services</a></li><br>
                    <li><a href="Contact.php">Contact</a></li><br>
                    <li><a href="">Navigation</a> </li>
                </ul>
            </div>  
            <div class="contenu3">
                <a href="#"> <img src="public\images\Facebook.png" alt="" width="17px" height="17px"></a>
                <a href="#"> <img src="public\images\Instagram2.png" alt="" width="17px" height="17px"></a>
                <a href="#"><img src="public\images\Twitter2.png" width="17px" height="17px"></a>
                <a href="#"><img src="public\images\LinkedIn2.png" width="17px" height="17px"></a>
                <a href="#"><img src="public\images\Youtube2.png" width="17px" height="17px"></a>
            </div>
            <div class="lien">
                <a href=""> <img src="public\images\campus.png" alt="" width="30px" height="30px"></a>
                <a id="link" href="http://www.acampus.academy/campus/aix-en-provence">http://www.acampus.academy/campus/aix-en-provence</a>
            </div>
        </footer>
        <script src="public/js/script.js"></script>
</body>
</html> 