<?php
try
{
    $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
    $db = new PDO('mysql:host=127.0.0.1; dbname=agence_com_con','root','');
    //champs en majuscule
    $db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
    //exception en cas d'erreur
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    die('Erreur : '.$e->getMessage());
}

?>