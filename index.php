<!DOCTYPE html>
<html lang="fr">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>AGENCEA</title>
        <link rel="shortcut icon" href="public\images\logoCom.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
       
   <style>
    body{
    background-image: url("public/images/FOND_ACCUEIL1.png");
    background-size: cover;
    top:0;
    padding: 0;
    margin:0;
    background-attachment: fixed;
    background-color: white;
    font-family: "Times New Roman", Times, "Liberation Serif", FreeSerif, serif;
    font-size: 12px; 
    background-repeat:no-repeat;
    box-sizing:border-box;
    overflow:hidden;
}

/* valable pour tous les écrans */

html, body{
    height: 100%;
  margin: 0;
     }
.navbar{
    min-height:10vh;
    margin: 4px;
    display: flex;
    justify-content: space-between; 
    align-items: center;
    background-color: white;
    color:  #FDC3AA; 
    position: fixed;
    }
.brand-tilte{
    font-size: 2rem;
    margin: 0rem;
    }
.navbar-link ul{
    flex:1 1 40rem;
    margin: 0;
    padding-right: 12px;
    display: flex;
    }
.navbar-link li{
    list-style: none;
    }  
.navbar-link li a{
    text-decoration: none;
    color:  #FDC3AA; 
    padding: 1.6rem;
    display: block;
    }
.navbar-link li:hover{
    background-color:#8FBC8F;
    }
    .nav-link{
        color:  #FDC3AA; 
    }
footer{
    padding-left: 1px;
    background-color: #FDC3AA;
    width: 100%;
    height: auto;
    position: absolute;
    bottom: 1px;
    left: 0px;
    color: white;
    }
 .contenu{
    margin: 3px;
    padding-left: 40px;
    }
 .contenu li{
    list-style: none;
    margin: -19px;
    padding-left: 10px;
    }
.droits{
    margin: -10px;
    padding-left: 10px;
    }
.navbar{
    margin: 0;
    width: 100%;
    padding-left: 25px;
    }
 .contenu1{
    margin: 4px;
    padding-left: 128px;
    }

 .contenu1 li{
    list-style: none;
    margin: -16px;
    padding-left: 50px;
    font-size: 8px;
}
.contenu2{
    margin: 5px;
    padding-left: 190px;
}
.contenu2 li{
    list-style: none;
    margin: -16.5px;
    padding-left: 80px;
    font-size: 9px;
}
.contenu2 li a{
    list-style: none;
    margin: -18px;
    padding-left: 80px;
    font-size: 9px;
    text-decoration: none;
    color:white;
    }
    .contenu3{
    margin: -27px;
    padding-left: 640px;
    }
 
.options li{
    padding:5px;
    font-size: 13px;
    list-style: none;
    text-decoration: none;
    text-decoration: overline solid #FF8C00 5px;
    }
    .options{
        padding:5px;
    }
.liText{
    margin:-47px;
    text-decoration: none;
    padding-left:150px;
    padding-right: 0px;
    font-size: 13px;
    
    }
.liText li{
    list-style: none;
    text-decoration: none; 
    text-decoration: overline solid #FF8C00 5px;
    }
.illusta{
    margin: -60px;
    padding-left: 90px
}
.text{
    margin: -75px;
    padding-left: 210px
    }
.titre{
    font-size: 20px;
    }
    .lien{
    margin: 10px;
    padding-left: 570px;
    font-size: 10px;
    color:white;
    }
.listeLi{
    list-style: none;  
    text-decoration: none; 
    display: block;
    align-items: right;
    margin-top: -340px;
    padding-left: 200px;
    }
.fond7{
    padding-left: 837.5px;
    }
    #link { color:white ; }
  

</style>
</head>
<body>
<nav class="navbar navbar-expand-md" style="background-color:white" > 
    <a href="#"><img src="public\images\logoCom.png" alt="" width="100px" height="65px"></a>
    <button type="button" class="navbar-toggler"><span class="navbar-toggler-icon px-6" ></span></button>    
    <ul class="navbar-nav">
            <li class="nav-item"><a  class="nav-link font-weight-bold text-uppercase px-5" href="#">Accueil</a></li>
            <li class="nav-item"><a  class="nav-link font-weight-bold text-uppercase px-5" href="NosServices.php">Nos Services</a></li>
             <li class="nav-item"><a class="nav-link font-weight-bold text-uppercase px-5" href="Contact.php">Contact</a></li>
             <li class="nav-item"><a class="nav-link font-weight-bold text-uppercase px-5" href=""><img src="public\images\drapeau.jpg" alt="" width="14px" height="14px">FR</a></li>  
        </ul>
    </div>
</nav>
<div class="bandeau">
<img src="public\images\BANDEAU6.png" alt="" width="100%" height="87px">
</div>

<div class="illusta">
<img src="public\images\ILLUSTRATION.png" alt="" width="790px" height="420px">
</div>  
<div class="text">
        <ul class="listeLi">
            <li>
                <p>
                <p class="titre"><strong> POURQUOI NOUS CHOISIR?</strong></p>
                    L'agence de communication et consulting vous accompagne <br>
                    de façon à optimiser votre commication d'entreprise pour <br>
                    qu'elle soit la plus efficace possible.Nous mettons tout en <br>
                    oeuvre pour perfectionner vos choix de support en fonction <br>
                    de vos interlocuteurs.
                </p>
            </li><br>
            <li>
                <ul class="options">
                    <li class="liText">PROFESSIONALISME</li>
                    <li class="liText">CONFIANCE</li>
                    <li class="liText">ENTRAIDE</li>
            
                </ul>
            </li>
            <ul class="liText">
                <li class="liText">EVOLUTION</li>
                <li class="liText">PARTAGE</li>
            </ul>
        </ul>
     </div> 
     <footer>
             <div class="contenu">
                   <img src="public\images\footer.png" alt="" width="40px" height="30px">
                    <ul class="listeLi_Footer">
                    <li>Tous droits réserbés</li>
                    <li>&copy; 2020 agencea </li>
                    </ul>      
            </div>  
            <div class="contenu1">
                 <ul>
                    <li>Mentions légale</li>
                    <li>Politique de Confidentialité</li>
                    <li>Statistiques et Performances</li>  
                    <li>Réclamation</li> 
                </ul>
            </div>
            <div class="contenu2">
                <ul classe="contenu02">
                     <li><a href="index.php">Accueil</a></li>
                     <li><a href="NosServices.php">Services</a></li>
                     <li><a href="Contact.php">Contact</a></li>
                     <li><a href="">Navigation</a> </li>
                </ul>
            </div>  
            <div class="contenu3">
                    <a href="#"> <img src="public\images\Facebook.png" alt="" width="17px" height="17px"></a>
                   <a href="#"> <img src="public\images\Instagram2.png" alt="" width="17px" height="17px"></a>
                   <a href="#"><img src="public\images\Twitter2.png" width="17px" height="17px"></a>
                   <a href="#"><img src="public\images\LinkedIn2.png" width="17px" height="17px"></a>
                   <a href="#"><img src="public\images\Youtube2.png" width="17px" height="17px"></a>
            </div>
            <div class="lien">
               <a href=""> <img src="public\images\campus.png" alt="" width="30px" height="30px"></a>
               <a id="link" href="http://www.acampus.academy/campus/aix-en-provence">http://www.acampus.academy/campus/aix-en-provence</a>
            </div>
    </footer>
</div>
<script src=""></script>
</body>
</html>